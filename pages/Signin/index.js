import React from 'react';
import { 
  KeyboardView, 
  Title, 
  Container, 
  Input, 
  PageLogo,
  ButtonSubmit,
  TextButton
} from './styles';
import Header from '../../components/Header';

function Signin() {
  return(
    <KeyboardView>
      <Header />
      <Container>
      <PageLogo resizeMode="cover" source={require('./../../assets/img/Maskotik.png')}/>
        <Title>Mascotik</Title>
        <Input 
          placeholderTextColor="#fff"
          placeholder="E-mail"
        />
        <Input 
          placeholderTextColor="#fff"
          placeholder="Password"
          secureTextEntry
        />
        <ButtonSubmit>
          <TextButton>
            Entrar
          </TextButton>
        </ButtonSubmit>
      </Container>
    </KeyboardView>
  )
}

export default Signin;